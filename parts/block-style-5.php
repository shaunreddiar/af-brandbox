<section class="content-wrapper block-style-5">
    <div class="container">
        <div class="icon-section-heading full-width"><img src="<?php echo getAssetsFolder() . "hear-icon.svg" ?>" alt="See">  Hear</div>
    </div>
    <div class="container flex-h">

        <div class="hear-blocks">
            <div class="hear-item">
                <img class="mobile-image " src="<?php echo getAssetsFolder() . "hero-wave.png"?>">
                <p class="font-bold color_8">The Hero</p>
                <p>The voice of the Alexforbes Hero sounds strong, confident and proud. The Hero's timbre is rich and resonant. The voice of the hero falls into the musical areas of bass (assertive) and baritone (calming).
                <p>Bravely spirited, they speak up and speak out for themselves and others.</p>
                <p>The Hero typifies the musical genre anthemic, uplifting and dramatic.</p>
                <p><a href="#">Click hear.</a></p>
            </div>
    
            <div class="hear-item image image-desktop hero" style="background-image: url('<?php echo getAssetsFolder() . "hero-wave.png" ?>')"></div>
    
            <div class="hear-item">
                <img class="mobile-image " src="<?php echo getAssetsFolder() . "every-wave.png"?>">
                <p class="font-bold color_11"> The Everyman</p>
                <p>The voice of the Alexforbes Everyman is warm, and friendly with an energetic mezzo (midtone) timbre.</p>
                <p>Open and warm, The everyman greets people by name or nick name and can resonate well on any frequency.</p>
                <p>They typify the music genre of homely, welcoming joyful.</p>
                <p><a href="#">Click hear.</a></p>
            </div>
    
            <div class="hear-item image image-desktop sage" style="background-image: url('<?php echo getAssetsFolder() . "sage-wave.png" ?>')"></div>
    
            <div class="hear-item">
                <img class="mobile-image " src="<?php echo getAssetsFolder() . "sage-wave.png"?>">
                <p class="font-bold color_2"> The Sage</p>
                <p>The voice of the Alexforbes Sage is calm, well balanced and assured. The timbre is honeyed, lyrical and smoky.</p>
                <p>Confident and well-versed, they express insights and anecdotes to share knowledge with others.</p>
                <p>They embody the music genre of earthy, inspiring tranquil.</p>
                <p><a href="#">Click hear.</a></p>
            </div>
    
            <div class="hear-item image image-desktop everyman" style="background-image: url('<?php echo getAssetsFolder() . "every-wave.png" ?>')"></div>
        </div>
        
    </div>
</section>