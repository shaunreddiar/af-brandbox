<section class="content-wrapper block-style-6">
    <div class="container flex-h">

        <div class="block-style-col hero">
        <div class="icon-section-heading show-only-mobile"><img src="<?php echo getAssetsFolder() . "hear-icon.svg" ?>" alt="See">  Hear</div>
            <div class="image-wrapper">
                <div class="img-clip-mask" style="background-image: url('<?php echo getAssetsFolder() . "taste-image.jpg" ?>')"></div>
            </div>
            <div class="text-block">
                <div class="title font-bold">The Hero</div>
                <div class="font-small">
                    The Hero of the plate (and of our hearts) is the mouth-watering roast - be it chargrilled beef or crispy potatoes and smoky veg. It evokes the gees, grit and courage of all Alexforbers around the world. The braai brings us together. Together we conquer all.
                </div>
            </div>
        </div>

        <div class="block-style-col sage drop-shadow">
            <div class="icon-section-heading show-only-desktop"><img src="<?php echo getAssetsFolder() . "hear-icon.svg" ?>" alt="See">  Hear</div>
            <div class="text-block">
                <div class="title font-bold">The Sage</div>
                <div class="font-small">
                    The Sage conjures a heady, herbal aroma, with a peppery tang with hints of mint, eucalyptus, and lemon. It is the taste of nature's most ancient and precious resources for us to savour, preserve and share. Because our impact today is our legacy for tomorrow.
                </div>
            </div>
            <div class="image-wrapper">
                <div class="img-clip-mask" style="background-image: url('<?php echo getAssetsFolder() . "taste-sage-image.jpg" ?>')"></div>
            </div>
        </div>

        <div class="block-style-col everyman">
            <div class="image-wrapper">
                <div class="img-clip-mask" style="background-image: url('<?php echo getAssetsFolder() . "taste-everyman-image.jpg" ?>')"></div>
            </div>
            <div class="text-block">
                <div class="h5 title font-bold">The Everyman</div>
                <div class="font-small">
                    The Everyman triggers an aroma of fresh- baked bread, a crispy outer crust with soft spongy core, heaping comfort and nourishment upon our souls. Breaking bread brings us together around the table to share stories and forge stronger connections.
                </div>
            </div>
        </div>

    </div>
</section>