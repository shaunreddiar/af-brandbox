<section class="content-wrapper block-style-8">
    <div class="container flex-h">
        
        <div class="block-style-col hero">
            <div class="icon-section-heading show-only-mobile"><img src="<?php echo getAssetsFolder() . "touch-icon.svg" ?>" alt="See"> Feel</div>
            <div class="image-wrapper">
                <div class="img-clip-mask" style="background-image: url('<?php echo getAssetsFolder() . "feel-hero-image.jpg" ?>')"></div>
            </div>
            <div class="text-block">
                <div class="title font-bold">The Hero</div>
                <div class="font-small">
                100% Genuine leather feels heroic - soft and yielding, yet flexible and slightly gritty to the touch. It is durable, timeless and wears better with age. Aged leather creates the patina which smooths out the original grain, leaving an impression of comfort and dependability.
                </div>
            </div>
        </div>

        <div class="block-style-col sage drop-shadow">
            <div class="icon-section-heading show-only-desktop"><img src="<?php echo getAssetsFolder() . "touch-icon.svg" ?>" alt="See"> Feel</div>
            <div class="text-block">
                <div class="title font-bold">The Sage</div>
                <div class="font-small">
                    <p>
                        The feel of fibrous paper beneath your fingers as you turn a page (or the smooth, cool glass as you tap your tablet), unlocks the reward of insight and knowledge.
                    </p>
                    <p>
                    The flow of energy through the touch of your fingertips fuels your mind, connecting dots, creating pathways to help you learn and grow and feel empowered.
                    </p>
                </div>
            </div>
            <div class="image-wrapper">
                <div class="img-clip-mask" style="background-image: url('<?php echo getAssetsFolder() . "feel-sage-image.jpg" ?>')"></div>
            </div>
        </div>

        <div class="block-style-col everyman">
            <div class="image-wrapper">
                <div class="img-clip-mask" style="background-image: url('<?php echo getAssetsFolder() . "feel-everyman-image.jpg" ?>')"></div>
            </div>
            <div class="text-block">
                <div class="h5 title font-bold">The Everyman</div>
                <div class="font-small">
                Skin on skin is the purest form of touch for the Everyman. The hug, the handshake, a pat on the back – these all carry the texture of warmth and encouragement. It is how we connect, give support, show compassion. At every age, for every human, the touch of a hand tell us that someone cares.
                </div>
            </div>
        </div>
    <div>
</section>