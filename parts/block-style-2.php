<section class="content-wrapper block-style-2">
    <div class="container flex-h">
        <div class="content">
            <div class="senses-blocks">
                <div class="block big no-1">
                    <img src="<?php echo getAssetsFolder() . "see-icon.svg" ?>" alt="see">
                </div>
                <div class="block-wrapper">
                    <div class="block no-2">
                        <img src="<?php echo getAssetsFolder() . "hear-icon.svg" ?>" alt="hear">
                    </div>
                    <div class="block no-3">
                        <img src="<?php echo getAssetsFolder() . "taste-icon.svg" ?>" alt="taste">
                    </div>
                    <div class="block no-4">
                        <img src="<?php echo getAssetsFolder() . "smell-icon.svg" ?>" alt="smell">
                    </div>
                    <div class="block no-5">
                        <img src="<?php echo getAssetsFolder() . "touch-icon.svg" ?>" alt="touch">
                    </div>
                </div>
            </div>
        </div>
        <div class="content text">
            <div class="content-title">
                Exbox
            </div>
            <p>Open your imagination. Sharpen your senses. Get ready for a stimulating sensory exploration of the Alexforbes archetypes.</p>
        </div>
    </div>
</section>