<section class="slider-style-1">
    <div class="container">
        Alexforbes has chosen 3 archetypes who typify our brand -

        <div class="slider-nav">
            <div data-slide-index="0" class="nav-item hero active">The Hero</div>
            <div data-slide-index="1" class="nav-item sage">The Sage</div>
            <div data-slide-index="2" class="nav-item everyman">The Everyman</div>
        </div>

        <div id="afb-slider">

            <!-- Slide 1 -->
            <div class="slide">
                <div class="afb-slide">
                    <div class="afb-slide-item image-1" style="background-image: url('<?php echo getAssetsFolder() . "hero-card-img-1.jpg" ?>')"></div>
                    <div class="afb-slide-item image-2" style="background-image: url('<?php echo getAssetsFolder() . "hero-card-img-2.jpg" ?>')"></div>
                    <div class="afb-slide-item text">
                        <div class="afb-slide-row">
                            <div class="slide-row-item">
                                <div class="slide-icon-sections">
                                    <div class="section heading h2">
                                        The Hero
                                    </div>
                                    <div class="section icon-text">
                                        <img src="<?php echo getAssetsFolder() . "archetype-heart-orange.svg"?>" alt="The Hero">Mastery
                                    </div>
                                    <div class="section icon-text font-italic">
                                        <img src="<?php echo getAssetsFolder() . "archetype-quote-orange.svg"?>" alt="Quote">Where there is a will, there is a way
                                    </div>
                                </div>
                            </div>
                            <div class="slide-row-item blurb">
                                The Hero archetype is courageous, inspiring, determined. They take pride in working hard to achieve results. They meet challenges head-on and correct their mistakes. Growth and development are key drivers, and they embrace change. The Hero strives to lead by example, while empowering others.
                            </div>
                        </div>
    
                        <div class="afb-slide-row">
                            <div class="slide-row-item panel">
                                <div class="panel-heading font-bold">Brand Voice</div>
                                Honest<br/>
                                Candid<br/>
                                Brave
                            </div>
    
                            <div class="slide-row-item panel">
                                <div class="panel-heading font-bold">Brand Message</div>
                                We can make the world better. We have the grit and determination to outwork the rest.
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
            <!-- Slide 1 ends -->

            <!-- Slide 2 -->
            <div class="slide">                
                <div class="afb-slide sage">
                    <div class="afb-slide-item image-1" style="background-image: url('<?php echo getAssetsFolder() . "sage-card-img-1.jpg" ?>')"></div>
                    <div class="afb-slide-item image-2" style="background-image: url('<?php echo getAssetsFolder() . "sage-card-img-2.jpg" ?>')"></div>
                    <div class="afb-slide-item text">
                        <div class="afb-slide-row">
                            <div class="slide-row-item">
                                <div class="slide-icon-sections">
                                    <div class="section heading h2">
                                        The Sage
                                    </div>
                                    <div class="section icon-text">
                                        <img src="<?php echo getAssetsFolder() . "archetype-heart-sage.svg"?>" alt="Heart">Understanding
                                    </div>
                                    <div class="section icon-text font-italic">
                                        <img src="<?php echo getAssetsFolder() . "archetype-quote-sage.svg"?>" alt="Quote">The truth will set you free.
                                    </div>
                                </div>
                            </div>
                            <div class="slide-row-item blurb">
                            The Sage archetype is a seeker of truth and understanding, and a trusted source of expertise. They are researchers and teachers at heart, who analyse patterns, numbers and data to find solutions. They share their knowledge generously and celebrate life long learning.
                            </div>
                        </div>
    
                        <div class="afb-slide-row">
                            <div class="slide-row-item panel">
                                <div class="panel-heading font-bold">Brand Voice</div>
                                Knowledgeable<br/>
                                Assured<br/>
                                Guiding
                            </div>
    
                            <div class="slide-row-item panel">
                                <div class="panel-heading font-bold">Brand Message</div>
                                Education is the path to wisdom and wisdom is where the answers lie.
                            </div>
                        </div>
                    </div>
                </div>
            </div>          
            <!-- Slide 2 ends -->

            <!-- Slide 3 starts -->
            <div class="slide">                
                <div class="afb-slide everyman">
                    <div class="afb-slide-item image-1" style="background-image: url('<?php echo getAssetsFolder() . "everyman-img-1.jpg" ?>')"></div>
                    <div class="afb-slide-item image-2" style="background-image: url('<?php echo getAssetsFolder() . "everyman-img-2.jpg" ?>')"></div>
                    <div class="afb-slide-item text">
                        <div class="afb-slide-row">
                            <div class="slide-row-item">
                                <div class="slide-icon-sections">
                                    <div class="section heading h2">
                                        The Everyman
                                    </div>
                                    <div class="section icon-text">
                                        <img src="<?php echo getAssetsFolder() . "archetype-heart-everyman.svg"?>" alt="Heart">Belonging
                                    </div>
                                    <div class="section icon-text font-italic">
                                        <img src="<?php echo getAssetsFolder() . "archetype-quote-everyman.svg"?>" alt="Quote">You're just like me and I'm just like you.
                                    </div>
                                </div>
                            </div>
                            <div class="slide-row-item blurb">
                            The Everyman archetype is authentic and wholesome; as fun and likeable as they come. They make people feel comfortable and understood. They are team players with a humble nature, and they inspire others with their positive energy.
                            </div>
                        </div>
    
                        <div class="afb-slide-row">
                            <div class="slide-row-item panel">
                                <div class="panel-heading font-bold">Brand Voice</div>
                                Friendly<br/>
                                Humble<br/>
                                Authentic
                            </div>
    
                            <div class="slide-row-item panel">
                                <div class="panel-heading font-bold">Brand Message</div>
                                When we treat each other with honesty and friendliness we can live together in harmony.
                            </div>
                        </div>
                    </div>
                </div>
            </div>           
            <!-- Slide 3 ends -->


        </div>
    </div>
</section>