<section class="content-wrapper block-style-4">
    <div class="container flex-h">
        <div class="icon-section-heading show-only-mobile"><img src="<?php echo getAssetsFolder() . "see-icon.svg" ?>" alt="See">  See</div>
        <div class="strip hero">
            <div class="text-block" style="background-image: url('<?php echo getAssetsFolder() . "hero-circle.png" ?>')">
                <div class="heading font-bold">The Hero</div>
                The Alexforbes brand manifesto captures the essence of who we are, our core purpose and how we empower our clients.
                <div class="video-block">
                    <video controls id="heroVideo" src="<?php echo getAssetsFolder() . 'media/video/manifesto-video-hero.mp4' ?>" type="video/mp4"></video>
                </div>
            </div>
        </div>
        <div class="strip sage">
            <div class="icon-section-heading show-only-desktop"><img src="<?php echo getAssetsFolder() . "see-icon.svg" ?>" alt="See">  See</div>

            <div class="text-block" style="background-image: url('<?php echo getAssetsFolder() . "sage-circle.png" ?>')">
                <div class="heading font-bold">The Sage</div>
                    Listen to the wisdom of Alexforbes insight pioneers, who expound on how the brand is going to grow our business.
                <div class="video-block">
                    <video controls id="sageVideo" src="<?php echo getAssetsFolder() . 'media/video/sakeholder-video-sage.mp4' ?>" type="video/mp4"></video>
                </div>
            </div>

        </div>
        <div class="strip everyman">
            <div class="text-block" style="background-image: url('<?php echo getAssetsFolder() . "everyman-circle.png" ?>')">
                <div class="heading font-bold">The Everyman</div>
                    This video illustrates about us. You, me and how we are positively disrupting the financial industry and impacting lives.
                <div class="video-block">
                    <video controls id="everymanVideo" src="<?php echo getAssetsFolder() . 'media/video/stomp-video-everyman.mp4' ?>" type="video/mp4"></video>
                </div>
            </div>
        </div>
    </div>
</section>