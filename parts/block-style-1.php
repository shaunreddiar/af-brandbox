<section class="content-wrapper block-style-1" style="background-image: url('/wp-content/themes/af-brandbox/assets/header-bg.jpg')">
    <div class="container flex-h">
        <div class="content circle">
            <div style="background-image: url('<?php echo getAssetsFolder() . "brandbox-circle-img.jpg"?>')" class="circle-image bg-img-cover"></div>
        </div>

        <div class="content text">
            <div class="content-title">
                <div class="content-title-icon">
                    <img src="<?php echo getAssetsFolder() . "brandbox-logo.png" ?>" alt="AF Brandbox" srcset="">
                </div>
                <span>BrandBox</span>
            </div>
            <p>Welcome! The <strong>Brandbox</strong> is a space for you to explore and immerse yourself in insights, inspiration and attributes that shape or refreshed brand.</p>
            <p>Choose your brand journey...<br/>
            Open our <strong>Toolbox</strong> to discover the Why? How? and What? behind our brand refresh. Help yourself to useful templates, references and marketing material here.</p>
            <p>And..
            Step into the playful arena of the <strong>Exbox</strong>, where you can experience the 3 brand archetypes through your 5 senses.</p>
        </div>
    </div>
</section>