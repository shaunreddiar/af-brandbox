<section class="content-wrapper block-style-7">
    <div class="container flex-v">
        <div class="icon-section-heading"><img src="<?php echo getAssetsFolder() . "smell-icon.svg" ?>" alt="Smell">  Smell</div>

        <div class="row hero">
            <div class="row-item image" style="background-image: url('<?php echo getAssetsFolder() . "smell-hero-image.jpg"?>')">
                <div class="circle-img" style="background-image: url('<?php echo getAssetsFolder() . "smell-perf.png"?>')"></div>
            </div>
            <div class="row-item text">
                <div class="content">
                    <p class="font-bold">The Hero</p>
                    
                    <p class="font-small">The Alexfores Hero fragrance comes from the leather family, known for its durability and its aesthetics. The scent is created from fragrant woods and dried birch bark, with a smoky infusion.</p>
                    <p class="font-small"> The aroma of leather is unmistakable, reminiscent of a product built to last. It is not just the fragrance, but the patina of leather that makes the Hero archetype so memorable.</p>
                </div>
            </div>
        </div>

        <div class="row sage">
            <div class="row-item text">
                <div class="content">
                    <p class="font-bold">The Sage</p>
                    
                    <p class="font-small">The Alexforbes Sage fragrance takes inspiration from the pages of books. Old books, new books, text books, hard cover and soft, all have a distinct smell. The organic compounds in pages release chemicals that smell like almond, vanilla and grass.</p>
                    <p class="font-small"> It is not only the smell of the book but the knowledge which flows off pages which informs this archetype’s perfume.</p>
                </div>
            </div>
            <div class="row-item image" style="background-image: url('<?php echo getAssetsFolder() . "smell-sage.jpg"?>')">
                <div class="circle-img" style="background-image: url('<?php echo getAssetsFolder() . "sage-perf.png"?>')"></div>
            </div>
        </div>

        <div class="row everyman">
            <div class="row-item image" style="background-image: url('<?php echo getAssetsFolder() . "smell-everyman-image.jpg"?>')">
                <div class="circle-img" style="background-image: url('<?php echo getAssetsFolder() . "everyman-perf.png"?>')"></div>
            </div>
            <div class="row-item text">
                <div class="content">
                    <p class="font-bold">The Everyman</p>
                    
                    <p class="font-small">The Alexforbes Everyman fragrance is based on aquatic or ozonic notes. It is can also be described as being “fresh out the shower” crisp, cool and clean. It evokes the fresh essence of the ocean, salty sea spray and the sun on your skin.</p>
                    <p class="font-small">It is gentle, yet alluring, and because it contains androgynous elements, it appeals to all genders. An everyday scent for every woman and every man.</p>
                </div>
            </div>
        </div>
    </div>

</div>