<section class="content-wrapper block-style-3">

    <div class="archetype-wheel show-only-mobile">
        <img src="<?php echo getAssetsFolder() . "archetype-wheel.png" ?>" alt="archetype-wheel">
    </div>
    
    <div class="container flex-h panel top">

        <div class="content">
            <div class="placeholder"></div>
            <div class="archetype-wheel show-only-desktop">
                <img src="<?php echo getAssetsFolder() . "archetype-wheel.png" ?>" alt="archetype-wheel">
            </div>
        </div>
        <div class="content text">
            <div class="content-title">
                Our Brand Archetypes
            </div>
            <p>Archetypes have always been used in story telling to help us recognise characters such as the hero, the innocent and the outlaw.</p>
            <p>In marketing, Carl Jung's 12 behavioural archetypes guide us in identifying our customer and brand with an ideal archetype.</p>
        </div>
    </div>

    <div class="panel bottom">
        <div class="container flex-h">
            <div class="content">
                <div class="placeholder"></div>
            </div>
            <div class="content text">
                <p>There are two primary reasons why we would align our brand to an archetype/s.</p>
                <p><strong>Connection</strong>: We need to make a deeper and more meaningful connection with our audience</p>
                <p><strong>Differentiation</strong>: We wish to establish a personality which is unique and memorable.</p>
            </div>
        </div>
    </div>
</section>