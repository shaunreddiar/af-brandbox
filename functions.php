<?php

// Enqueue custom css
function theme_styles() {
    wp_enqueue_style('af_brandbox_css', get_stylesheet_directory_uri() . '/styles/theme-style.css');
    wp_enqueue_style('slick_css', get_stylesheet_directory_uri() . '/scripts/plugins/slick/slick/slick.css');
}
add_action('wp_enqueue_scripts', 'theme_styles' );

// Enqueue custon JS files
function theme_scripts() {
    wp_enqueue_script('slick-slider', get_stylesheet_directory_uri() . '/scripts/plugins/slick/slick/slick.min.js' , array('jquery'));
    wp_enqueue_script('af-brandbox-scripts', get_stylesheet_directory_uri() . '/scripts/af-brandbox.js' , array('slick-slider'), null, true);
}

add_action('wp_enqueue_scripts', 'theme_scripts' );


function getAssetsFolder() {
    return "/wp-content/themes/af-brandbox/assets/";
}