jQuery(function($) {
    $(document).ready(function($) {
        
        const archetypeSlider = $('#afb-slider');
        const slideNavItems = $('.nav-item');
        
        // Video media
        const everyManVideo = $('#everymanVideo');
        const sageVideo = $('#sageVideo');
        const heroVideo = $('#heroVideo');
        const scrollTopButton = $('#scrollTop');

        // Slider starts
        if(archetypeSlider) {
            archetypeSlider.slick({
                dots: false,
                arrows: false,
                autoplay: false,
                infinite: false,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: false,
                variableWidth: false,
                adaptiveHeight: false,
                slide: '.slide'
            });

            archetypeSlider.on('afterChange', (slick, e) => {
                const currentSlideIndex = e.currentSlide;
                slideNavItems.removeClass('active');
                const query = '.nav-item[data-slide-index="' + currentSlideIndex + '"]'
                const activeNav = $(query);
                activeNav.addClass('active');
            });

            slideNavItems.click((e) => {
                const goToSlide = $(e.target).data('slide-index');
                archetypeSlider.slick('slickGoTo', goToSlide);
            });
        }
        // Slider ends


        // Handle video playback
        if(everyManVideo) {
            everyManVideo.on('playing', () => {
                if(sageVideo) {
                    sageVideo.trigger('pause');
                }
                if(heroVideo) {
                    heroVideo.trigger('pause');
                }
            });
        }

        if(sageVideo) {
            sageVideo.on('playing', () => {
                if(everyManVideo) {
                    everyManVideo.trigger('pause');
                }
                if(heroVideo) {
                    heroVideo.trigger('pause');
                }
            });
        }

        if(heroVideo) {
            heroVideo.on('playing', () => {
                if(everyManVideo) {
                    everyManVideo.trigger('pause');
                }
                if(sageVideo) {
                    sageVideo.trigger('pause');
                }
            });
        }

        if(scrollTopButton) {
            scrollTopButton.click(function() {
                $("html, body").animate({ scrollTop: 0 }, "slow");
            });
        }


    });
});