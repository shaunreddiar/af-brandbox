<?php get_header(); ?>

<div class="afb-content">
    <?php get_template_part('parts/block', 'style-1'); ?>
    <?php get_template_part('parts/block', 'style-2'); ?>
    <?php get_template_part('parts/block', 'style-3'); ?>
    <?php get_template_part('parts/slider', 'style-1'); ?>
    <?php get_template_part('parts/block', 'style-4'); ?>
    <?php get_template_part('parts/block', 'style-5'); ?>
    <?php get_template_part('parts/block', 'style-6'); ?>
    <?php get_template_part('parts/block', 'style-7'); ?>
    <?php get_template_part('parts/block', 'style-8'); ?>
    
</div>

<?php get_footer(); ?>