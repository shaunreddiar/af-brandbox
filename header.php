<!DOCTYPE html>
<html id="afBranbrox-site" <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=deive-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title><?php bloginfo( 'name' ); ?></title>
    <?php wp_head() ?>
</head>

<header class="afb-site-header">
    <div class="container flex-h">
        <div class="afb-header-logo">
            <img src="<?php echo  getAssetsFolder() . "logo.png"?>" >
        </div>
        <div class="afk-header-nav">
            <a class="nav-item" href="#">Experience Alexforbes</a>
            <a class="nav-item" href="#">Brandbox</a>
        </div>
    </div>
</header>